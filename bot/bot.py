import os
import requests
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, CallbackContext

TELEGRAM_TOKEN = "TOKEN"
ESP_URL = "http://192.168.1.100"

def start(update: Update, context: CallbackContext):
    keyboard = [
        [
            InlineKeyboardButton("GPIO2 ON", callback_data='gpio2_on'),
            InlineKeyboardButton("GPIO2 OFF", callback_data='gpio2_off'),
        ],
        [
            InlineKeyboardButton("GPIO0 ON", callback_data='gpio0_on'),
            InlineKeyboardButton("GPIO0 OFF", callback_data='gpio0_off'),
        ],
        [
            InlineKeyboardButton("GPIO4 ON", callback_data='gpio4_on'),
            InlineKeyboardButton("GPIO4 OFF", callback_data='gpio4_off'),
        ],
        [
            InlineKeyboardButton("Reset", callback_data='reset'),
            InlineKeyboardButton("DHT Data", callback_data='dht_data'),
        ],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Please choose:', reply_markup=reply_markup)

def button(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()

    action = query.data
    if action == 'dht_data':
        response = requests.get(f"{ESP_URL}/dht")
        query.edit_message_text(text=f"DHT Data: {response.text}")
    elif action == 'reset':
        response = requests.get(f"{ESP_URL}/reset")
        query.edit_message_text(text="Controller reset")
    else:
        response = requests.get(f"{ESP_URL}/{action}")
        query.edit_message_text(text=f"Action {action} performed: {response.status_code}")

def main():
    updater = Updater(TELEGRAM_TOKEN)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CallbackQueryHandler(button))

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
