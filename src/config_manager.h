#ifndef CONFIG_MANAGER_H
#define CONFIG_MANAGER_H

#include <ArduinoJson.h>

class ConfigManager {
public:
    void begin();
    void saveConfig();
    void loadConfig();
};

#endif