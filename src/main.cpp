#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include "DHT.h"
#include "secrets.h"
#include "config_manager.h"
#include "relay_handler.h"
#include "dht_sensor.h"

#define DHTPIN 8  
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);
ESP8266WebServer server(80);
ConfigManager configManager;
RelayHandler relayHandler(1, 3, 4, 5);
DHTSensor dhtSensor(DHTPIN, DHTTYPE);

bool shouldSaveConfig = false;

void handleRoot() {
    server.send(200, "text/plain", "Hello, you are connected to ESP8266");
}

void saveConfigCallback() {
    Serial.println("Configuration has been updated");
    shouldSaveConfig = true;
}

void setup() {
    Serial.begin(115200);
    dht.begin();

    const char* ssid = "Configuration";
    const char* password = NULL;
    WiFi.softAP(ssid, password);

    IPAddress apIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(apIP);

    configManager.begin();
    relayHandler.begin();

    server.on("/", handleRoot);
    server.on("/gpio2/on", HTTP_GET, []() { relayHandler.handleRelayOn(1); });
    server.on("/gpio2/off", HTTP_GET, []() { relayHandler.handleRelayOff(1); });
    server.on("/gpio0/on", HTTP_GET, []() { relayHandler.handleRelayOn(3); });
    server.on("/gpio0/off", HTTP_GET, []() { relayHandler.handleRelayOff(3); });
    server.on("/gpio4/on", HTTP_GET, []() { relayHandler.handleRelayOn(4); });
    server.on("/gpio4/off", HTTP_GET, []() { relayHandler.handleRelayOff(4); });

    server.on("/dht", HTTP_GET, []() {
        float temperature = dht.readTemperature();
        float humidity = dht.readHumidity();
        if (isnan(temperature) || isnan(humidity)) {
            server.send(500, "text/plain", "Failed to read from DHT sensor");
        } else {
            String message = "Temperature: " + String(temperature) + " *C\nHumidity: " + String(humidity) + " %";
            server.send(200, "text/plain", message);
        }
    });

    server.on("/reset", HTTP_GET, []() {
        WiFi.softAPdisconnect(true);
        WiFi.disconnect(true);
        ESP.restart();
    });

    server.begin();
}

void loop() {
    server.handleClient();
    dhtSensor.checkHumidityAndControlRelay(5, 30, 45);
}
