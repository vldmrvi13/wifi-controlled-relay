#include "config_manager.h"
#include <FS.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

StaticJsonDocument<256> config;

void ConfigManager::begin() {
    if (SPIFFS.begin()) {
        Serial.println("Mounted file system");

        if (SPIFFS.exists("/config.json")) {
            Serial.println("Reading config file");
            File configFile = SPIFFS.open("/config.json", "r");
            if (configFile) {
                size_t size = configFile.size();
                std::unique_ptr<char[]> buf(new char[size]);
                configFile.readBytes(buf.get(), size);
                DeserializationError error = deserializeJson(config, buf.get());
                if (!error) {
                    Serial.println("Parsed config file");
                    // Подключение к сохраненной Wi-Fi сети
                    const char* ssid = config["ssid"];
                    const char* password = config["password"];
                    WiFi.begin(ssid, password);
                    while (WiFi.status() != WL_CONNECTED) {
                        delay(500);
                        Serial.print(".");
                    }
                    Serial.println("");
                    Serial.println("Connected to saved Wi-Fi network");
                } else {
                    Serial.println("Failed to parse config file");
                }
            }
        } else {
            Serial.println("Config file does not exist");
        }
    } else {
        Serial.println("Failed to mount file system");
    }
}

void ConfigManager::saveConfig() {
    Serial.println("Saving config");
    File configFile = SPIFFS.open("/config.json", "w");
    if (configFile) {
        serializeJson(config, configFile);
        configFile.close();
    }
}
