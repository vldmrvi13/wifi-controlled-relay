#include "dht_sensor.h"
#include <Arduino.h>

DHTSensor::DHTSensor(int pin, int type) : _dht(pin, type) {
    _dht.begin();
}

void DHTSensor::checkHumidityAndControlRelay(int relayPin, float minHumidity, float maxHumidity) {
    float humidity = _dht.readHumidity();
    if (isnan(humidity)) {
        Serial.println("Failed to read from DHT sensor!");
        return;
    }
    
    if (humidity < minHumidity) {
        digitalWrite(relayPin, LOW); // Включаем реле
    } else if (humidity > maxHumidity) {
        digitalWrite(relayPin, HIGH); // Выключаем реле
    }
}
