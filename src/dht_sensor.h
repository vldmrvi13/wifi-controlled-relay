#ifndef DHT_SENSOR_H
#define DHT_SENSOR_H

#include "DHT.h"

class DHTSensor {
public:
    DHTSensor(int pin, int type);
    void checkHumidityAndControlRelay(int relayPin, float minHumidity, float maxHumidity);

private:
    DHT _dht;
};

#endif