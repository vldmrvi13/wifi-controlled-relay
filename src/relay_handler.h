// relay_handler.h
#ifndef RELAY_HANDLER_H
#define RELAY_HANDLER_H

#include <ESP8266WebServer.h>

class RelayHandler {
public:
    RelayHandler(int gpio2, int gpio0, int gpio4, int gpio5);
    void begin();
    void handleRelayOn(int pin);
    void handleRelayOff(int pin);

private:
    int _gpio2, _gpio0, _gpio4, _gpio5;
};

#endif


