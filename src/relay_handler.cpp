// relay_handler.cpp
#include "relay_handler.h"
#include <Arduino.h>

RelayHandler::RelayHandler(int gpio2, int gpio0, int gpio4, int gpio5) 
    : _gpio2(gpio2), _gpio0(gpio0), _gpio4(gpio4), _gpio5(gpio5) {}

void RelayHandler::begin() {
    pinMode(_gpio2, OUTPUT);
    pinMode(_gpio0, OUTPUT);
    pinMode(_gpio4, OUTPUT);
    pinMode(_gpio5, OUTPUT);
}

void RelayHandler::handleRelayOn(int pin) {
    digitalWrite(pin, LOW); 
}

void RelayHandler::handleRelayOff(int pin) {
    digitalWrite(pin, HIGH); 
}